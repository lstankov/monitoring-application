# OpenCensus

```sh
#Requirements:
-C++11 comiler
-Bazel
-Prometheus

#OpenCensus C++ library requires the Bazel build system. To install Bazel on Centos 7 do the following:


# Download .repo file from : https://download.copr.fedorainfracloud.org/results/vbatts/bazel/epel7
# Find the lastest version and copy the file to /etc/yum.repos.d and install bazel

$wget https://copr-be.cloud.fedoraproject.org/results/vbatts/bazel/epel-7-x86_64/01330079-bazel3/bazel3-3.0.0-1.el7.x86_64.rpm
$sudo cp bazel3-3.0.0-1.el7.x86_64.rpm /etc/yum.repos.d/
$cd /etc/yum.repos.d
$sudo yum install java-11-openjdk-devel
$sudo rpm -Uvh bazel3-3.0.0-1.el7.x86_64

```

#To install and start Prometheus 
```sh
$wget https://github.com/prometheus/prometheus/releases/download/v2.17.1/prometheus-2.17.1.linux-amd64.tar.gz
$tar xvfz prometheus-*.tar.gz
$cd prometheus-*
#starting prometheus
$./prometheus --config.file=prometheus.yml

```
#To run and build metrics.cc
```sh
#Go to prometheus directory and change config file
$vi prometheus.yml
#put this in the file
scrape_configs:
  - job_name: 'ocquickstartcpp'

    scrape_interval: 10s

    static_configs:
      - targets: ['localhost:8888']

#start prometheus in separate terminal
$./prometheus --config.file=prometheus.yml

# return to directory where are BUILD, WORKSPACE and metrics.cc and run
$bazel build :metrics && ./bazel-bin/metrics

# Go to http://localhost:8888/metrics
# Navigate to the Prometheus UI at http://localhost:9090
``

