## Prototype application using python http requests

## Before running prototype application make sure you have installed python3, and set proxy (--proxy=http://np04-webgw1.cern.ch:3128) 
```sh
## Tools needed for running the scripts
sudo yum install python3*
sudo pip3 install requests
sudo pip3 install matplotlib
sudo pip3  install --upgrade setuptools
sudo pip3  install grequests
sudo pip3  install l ratelimit
sudo pip3 install --upgrade pip
sudo pip3  install pandas
sudo pip3  install influxdb


#Run the script
#example
#python3 python-request-app.py number-of-metrics number-of-records-per-metric time-in-minutes host-name
#python3 python-start-subprocess.py number-of-subprocesses number-of-metrics number-of-records-per-metric time-in-minutes host-name
python3 python-request-app.py 100 100 10000 np04-srv-030
python3 python-request-app_client_2.py 100 100 10000 np04-srv-030
python3 python-start-subprocess.py 10 100 100 10000 np04-srv-030

```


## Run the ansible-scripts 
```sh
sudo ansible-playbook 1.prometheus_installation.yml --extra-vars "version='2.19.2'"
sudo ansible-playbook 2.influxdb_installation.yml 
sudo ansible-playbook 3.restart_prometheus.yml --extra-vars "version='2.19.2'"
sudo ansible-playbook 4.start_grafana.yml --extra-vars "version='7.0.5'"

```


----
