cmake_minimum_required(VERSION 3.15)
project(opmlib VERSION 1.0.0)


#set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../daq-buildtools/cmake ${CMAKE_MODULE_PATH})
#include(DAQ)
find_package(daq-buildtools 1.2.1 REQUIRED)


daq_setup_environment()

find_package(ers REQUIRED)
find_package(TRACE REQUIRED)
#find_package(pistache REQUIRED)
find_package(Boost COMPONENTS unit_test_framework program_options REQUIRED)


#message("Pistache LIB -> " ${PISTACHE_LIB})
#message("Pistache INC -> " ${PISTACHE_INC})

set(OPMLIB_DEPENDENCIES  ers::ers)


##############################################################################
#daq_point_build_to( src )


daq_add_library(*.cpp LINK_LIBRARIES ${OPMLIB_DEPENDENCIES})

#add_library(opmlib src/metric_registry.cpp)
#target_link_libraries(opmlib ${OPMLIB_DEPENDENCIES})
#target_include_directories(opmlib PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}> )
#
#add_library(opmlib_metric_publish src/metric_publish.cpp)
#target_link_libraries(opmlib_metric_publish opmlib)
#
#add_library(opmlib_metric_monitor src/metric_monitor.cpp)
#target_link_libraries(opmlib_metric_monitor opmlib)
#
#add_library(opmlib_data_retrieval src/data_retrieval.cpp)
#target_link_libraries(opmlib_data_retrieval opmlib)


##############################################################################
#daq_point_build_to( test )

daq_add_application(opmlib_test opm_test.cpp TEST LINK_LIBRARIES opmlib)
daq_add_application(opmlib_retrieval opm_retrieval.cpp TEST LINK_LIBRARIES opmlib)


#add_executable(
#
#add_library(opmlib_test test/opm_test.cpp)
#target_link_libraries(opmlib_test opmlib)
#
#add_library(opmlib_retrieval test/opm_retrieval.cpp)
#target_link_libraries(opmlib_retrieval opmlib)
#
#
#
###############################################################################
#daq_point_build_to( unittest )
#
#daq_install(TARGETS
#  opmlib
#  opmlib_test
#  opmlib_retrieval
#)

daq_install()





