## Operational Monitoring C++ Library

## Before buiding the  library make sure to create a software work area
see https://github.com/DUNE-DAQ/appfwk/wiki/Compiling-and-running

```sh

clone this repo



## Build the software
```sh
. ./setup_build_environment
./build_daq_software.sh --clean --install --pkgname opmlib

```


## Run  the demos in another shell
```sh
. ./setup_runtime_environment
cd build/opmlib/test/apps
opmlib_test



```



----