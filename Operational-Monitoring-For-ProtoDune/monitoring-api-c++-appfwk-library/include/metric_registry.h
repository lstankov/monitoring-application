


#ifndef METRIC_REGISTRY_H_
#define METRIC_REGISTRY_H_


#include <iostream>
#include <string>
#include <map>
#include <atomic>
#include <functional>
#include <any>
#include <mutex>  
#include <shared_mutex>
#include <thread>
#include <set>


class MetricRefInterface
{
public:
    virtual ~MetricRefInterface() = 0;
    virtual  const std::string getTypeName()=0;

};
inline MetricRefInterface::~MetricRefInterface() {}


typedef std::shared_ptr<MetricRefInterface> MetricPtr;


template <typename T>
class MetricRef: public MetricRefInterface
{
public:

    MetricRef( T& ref) :	metric_ref_{ std::reference_wrapper<T>(ref)} {

     }
    virtual ~MetricRef() {
    }
    std::reference_wrapper<T> getValue(){
        return metric_ref_;
    }
    const std::string getTypeName(){
        return std::string(typeid(T).name());
    }



private:
	std::reference_wrapper<T> metric_ref_;
};



class MetricRegistry  {

private:
     MetricRegistry(){}
public:
    static MetricRegistry& getInstance(){
        static MetricRegistry theInstance;
        return theInstance;
    }

    size_t count() const;
    
    
    //mutable std::shared_mutex metrics_mutex_; /**< mutex that protects both MetricSets and metric_names. */
    typedef std::map<std::string, MetricPtr> MetricSet;
    MetricSet metric_set;
    typedef std::set<std::string> StringSet;
    StringSet metric_names_;

    template <typename T>
    void registerMetric(const std::string& metricName, 	std::reference_wrapper<T> myMetric);
    
    void unregisterMetric(const std::string& metricName);
    
    template <typename T>
    void getValueOfMetric(const std::string& metricName);

    std::map<std::string, MetricPtr> getMetrics();



  
};





#endif /* METRIC_REGISTRY_H_ */





