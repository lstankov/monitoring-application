
## Operational Monitoring C++ Library

## Before buiding the prototype library make sure you have built Pistache 
```sh
git clone https://github.com/oktal/pistache.git
cd pistache
mkdir build
cd build
cmake ..


## Build the libray 
```sh
mkdir build
cd build
cmake .. -DPISTACHE_INC=/path-to/pistache/include -DPISTACHE_LIB=/path-to/pistache/build/src
make
 

```


## Run the test 
```sh
./opmApp

```



----
