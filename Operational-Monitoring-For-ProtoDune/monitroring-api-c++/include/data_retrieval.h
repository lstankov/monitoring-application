


#ifndef DATA_RETRIEVAL_H_
#define DATA_RETRIEVAL_H_


#include <iostream>
#include <string>
#include <map>
#include <atomic>
#include <functional>
#include <any>
#include <mutex>  
#include <shared_mutex>
#include <thread>
#include <set>



class DataRetrieval{
public:


     void setDataRetrieval(int port, const std::string& database_name, const std::string& influxdb_uri);	 
     void retrieveValue(const std::string& metricName);
     void retrieveValueByHTTPRequest(const std::string& metricName);
     const int getPort() {return port;}
     const std::string& getDatabaseName(){return database_name;}
     const std::string& getDatabaseHostAddress(){return influxdb_uri;}


private:
     int port;
     std::string database_name;
     std::string influxdb_uri;
  
};




#endif /* DATA_RETRIEVAL_H_ */





