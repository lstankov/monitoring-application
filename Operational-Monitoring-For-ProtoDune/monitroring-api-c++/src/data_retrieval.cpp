#include <iostream>
#include <string>
#include <map>
#include <atomic>
#include <functional>
#include <any>
#include <mutex>  // For std::unique_lock
#include <shared_mutex>
#include <thread>
#include <set>

#include "data_retrieval.h"
#include "influxdb.hpp"

#include <stdio.h>
#include <curl/curl.h>


void DataRetrieval::setDataRetrieval(int portNumber, const std::string& databaseName, const std::string& influxdbUri){
	port=portNumber;
	database_name=databaseName;
        influxdb_uri=influxdbUri;
    

}


void DataRetrieval::retrieveValue(const std::string& metricName){
    	influxdb_cpp::server_info si("127.0.0.1", getPort(), getDatabaseName());
	std::string resp;
	influxdb_cpp::query(resp, "select * from "+  metricName, si);
	std::cout<< resp<< "\n";;
   
    }
    

//curl -G 'http://localhost:8086/query?pretty=true' --data-urlencode "db=prometheus_lola" --data-urlencode "q=SELECT value FROM cpu_load_short"


void DataRetrieval::retrieveValueByHTTPRequest(const std::string& metricName){

CURL *curl;
CURLcode res;

std::string temp;
temp = "http://localhost:"+ std::to_string(getPort()) + "/query?pretty=true --data-urlencode 'db="+getDatabaseName() + "' --data-urlencode 'q=SELECT value FROM "+metricName + "'";

curl = curl_easy_init();

if(curl) {

    curl_easy_setopt(curl, CURLOPT_URL, temp.c_str());

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if(res != CURLE_OK)
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));

    /* always cleanup */
    curl_easy_cleanup(curl);
}
}
