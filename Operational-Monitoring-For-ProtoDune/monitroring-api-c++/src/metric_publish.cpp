#include <iostream>
#include <string>
#include <map>

#include <mutex>  
#include <shared_mutex>
#include <thread>
#include <unistd.h>
#include <future>
#include <set>
#include <chrono>
#include <cstdint>


#include "CCMValid.hpp"
#include "CtrlNode.hpp"

using namespace std::chrono_literals;
using namespace dune::daq;
using namespace std;
using namespace std::chrono;

#include <stdio.h>
#include <curl/curl.h>

/*#include "json.hpp"
using namespace std;
using namespace nlohmann;*/

#include "metric_publish.h"
#include "HTTPRequest.hpp"
#include "influxdb.hpp"


uint64_t timeSinceEpochMillisec() {
  using namespace std::chrono;
  return duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count();
}


void MetricPublish::setPublisher( const std::string& pt,int portNumber, const std::string& databaseName, const std::string& influxdbUri){
	port=portNumber;
	database_name=databaseName;
        influxdb_uri=influxdbUri;
        publishTarget=pt;
    

}	 


void MetricPublish::publishMetric(const std::string& metricName, const std::string& application_name, const std::string& host_name,double metric_value){


influxdb_cpp::server_info si("127.0.0.1", getPort(), getDatabaseName());
     influxdb_cpp::builder()
    .meas(metricName)
    .tag("host",  host_name)
    .tag("application", application_name)
    .field("x", metric_value)
    .timestamp(timeSinceEpochMillisec())
    .post_http(si);

     
 } 


void MetricPublish::publishMetricByHTTP_Request(const std::string& metricName, const std::string& application_name, const std::string& host_name,double metric_value)
{
//curl -i -XPOST 'http://localhost:8086/write?db=prometheus_lola' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 143405556200000'
//curl -d 'measurement,tag1=value1,tag2=value2 field1=123,field2=1.23' -X POST 'http://localhost:8428/write'
//curl -G 'http://localhost:8428/api/v1/export' -d 'match={__name__=~"measurement_.*"}'
 CURL *curl;
 CURLcode res;
 
  curl_global_init(CURL_GLOBAL_ALL);
  //std::string uri="http://"+ getInfluxDbUrl()+ ":" + std::to_string(getPort()) + "/write"; // VictoriaMetrics
  std::string uri="http://"+ getInfluxDbUrl()+ ":" + std::to_string(getPort()) + "/write"+"?db="+getDatabaseName();


  std::string data_binary=metricName+",host="+host_name+",application="+application_name+" value="+std::to_string(metric_value)+" "+ std::to_string(timeSinceEpochMillisec());
  std::cout<<uri<< '\n';
    
  curl = curl_easy_init();
  if(curl) {
      curl_easy_setopt(curl, CURLOPT_URL, uri.c_str());

    //curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "cpu_load_short,host=server01,region=us-west value=0.64");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data_binary.c_str());

 
    res = curl_easy_perform(curl);
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 
    curl_easy_cleanup(curl);
  }
  curl_global_cleanup();


} 



   

void MetricPublish::ccm_publishMetric(const std::string& metricName, const std::string& application_name, const std::string& host_name,double metric_value){

    dune::daq::ccm::CtrlNode node;
    std::string uri="/write?db="+getDatabaseName();
    std::string data_binary=metricName+",host="+host_name+","+"application="+application_name+" value="+std::to_string(metric_value);
 

  try {
    const uint16_t port1 = ccm::Valid::portNumber(getPort());
     std::cout << "Port number: " << getPort() << '\n';
     std::cout << "Database name: " << getDatabaseName() << '\n';
     node.addClient("client1", "localhost", port1); 

  } catch (std::exception e) {
    std::cout << "Could not add controlled/controller entity: " << e.what() << '\n';
  } 

  std::cout << "Going to sleep 2s...\n";
  std::this_thread::sleep_for(2s);   
  std::string content = uri + data_binary;

  /*
  std::string jsonstr = "{\"name\":\"hd_used\",\"columns\":[\"value\", \"host\", \"mount\"],\"points\":[[23.2, \"serverA\", \"/mnt\"]]}"; 
  json js = json::parse(jsonstr);
  auto& cli = node.getClient("client1");*/


  try {
    node.sendCommand("client1", data_binary);
  } catch (std::exception e) {
    std::cout << "Couldn't send command to  controller client: " << e.what() << '\n';
  }

  node.teardown();

}


void MetricPublish::ccm_publishMetricThreads(const std::string& metricName, const std::string& application_name, const std::string& host_name,double metric_value){

  dune::daq::ccm::CtrlNode node;
  std::cout << "Port number: " << getPort() << '\n';
  std::cout << "Database name: " << getDatabaseName() << '\n';

  try {
    const uint16_t port1 = ccm::Valid::portNumber(getPort());
    node.addClient("client1", getInfluxDbUrl(), port1);

  } catch (std::exception e) {
    std::cout << "Could not add controlled/controller entity: " << e.what() << '\n';
  } 

  std::atomic<bool> should_run{true};;
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  auto td = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin);
  std::thread timer([&](){
    std::cout << "Timer starts...\n";
    begin = std::chrono::steady_clock::now();
    while(true) {
      std::this_thread::sleep_for(50us);
      td = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin);
      if (td.count() > 1000) {
        should_run = false;
        break;
      }
    }
    std::cout << "Timer joins...\n";
  });
  timer.detach();
  
  size_t ops = 0;

  while(should_run) {
    try {
      std::string sk("influx use" + getDatabaseName() + " insert " + metricName + " " + to_string(metric_value));
      node.sendCommand("client1", sk);
      ++ops;
    } catch (std::exception e) {
      std::cout << "Couldn't send command to  controller client: " << e.what() << '\n';
    }

  }
 

  std::cout << "Should run: " << should_run << " runTime: " << td.count() << "[ms] Ops:" << ops << '\n';
  auto req_count = node.getRequestCount("client1");
  std::cout << " Request count from client: +" << req_count.first << " failed:" << req_count.second << '\n';

  std::this_thread::sleep_for(5s);

  node.teardown();


}



/*
void MetricPublish::publishMetricByHTTP_Request(const std::string& metricName, const std::string& application_name, const std::string& host_name,double metric_value){

//curl -i -XPOST 'http://localhost:8086/write?db=prometheus_lola' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 143405556200000'
try
	{
        std::string uri="http://localhost:"+ std::to_string(getPort()) + "/"+ "write?db="+getDatabaseName();
	std::string data_binary=metricName+",host="+host_name+",application="+application_name+" value="+std::to_string(metric_value);
        std::map<std::string, std::string> parameters = {{"test_metric_app,host=server01,region=us-west value=", "1"}};
    	http::InternetProtocol protocol = http::InternetProtocol::V6;

	http::Request request(uri, protocol);
        const http::Response response = request.send("POST", data_binary.c_str(), {
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: runscope/0.1"
        });


        std::cout << std::string(response.body.begin(), response.body.end()) << '\n'; // print the result
	}
	catch (const std::exception& e)
	{
    		std::cerr << "Request failed, error: " << e.what() << '\n';
	}
     

} */







