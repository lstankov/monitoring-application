#!/usr/bin/env python3



#import requests
#import grequests
import random
import time
from datetime import datetime
from influxdb import InfluxDBClient
import uuid
import sys





def publish(count_measurements, count_records, ttime):
   id_tags = []
   for i in range(100):
      id_tags.append(str(uuid.uuid4()))
   #ip address of the remote storage not a name
   client = InfluxDBClient(host='10.73.138.31', port=8086)
   client.create_database('prometheus_lola')
   measurements=[]
   number_of_points = 250000 #count_records
   data_end_time = int(time.time() * 1000) #milliseconds
   data = []
   payloads = []
   current_point_time = data_end_time
   time_one_year_ago = 1568633702000
   while current_point_time > time_one_year_ago:
      for i in range(count_measurements):
         measurement_name = 'daq-'+str(int(sys.argv[5]))+'-metric-'+str(i)
         for j in range(number_of_points):
            sec_offset = j * 1000  # 1 second jump
            current_point_time = current_point_time - sec_offset
            data.append("{measurement},host={host},process={process},id={id} x={x},y={y},z={z}i {timestamp}"
            .format(measurement=measurement_name,
                    host=str(sys.argv[4]),
                    process=str(int(sys.argv[5])),
                    id=random.choice(id_tags),
                    x=round(random.random(),4),
                    y=round(random.random(),4),
                    z=random.randint(0,50),
                    timestamp=current_point_time))

         client_write_start_time = time.perf_counter()
         client.write_points(data, database='prometheus_lola', time_precision='ms', batch_size=10000, protocol='line')
         client_write_end_time = time.perf_counter()
         print("Application Write: {time}s".format(time=client_write_end_time - client_write_start_time))

#count = input ("Please enter the number of metrics per process:")
#count = int(count)
count_measurements = int(sys.argv[1])
print(count_measurements)

#count = input ("Please enter the number of records in one metric:")
#count = int(count_records)
count_records = int(sys.argv[2])
print(count_records)


#ttime = input ("Please enter the total time in minutes:")
#ttime = int(ttime)*60
ttime = int(sys.argv[3])*60
print(ttime)



publish(count_measurements, count_records, ttime)




