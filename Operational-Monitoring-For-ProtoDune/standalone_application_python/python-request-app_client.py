import requests
import grequests
import random
import time
from datetime import datetime
from influxdb import InfluxDBClient
import uuid
import sys


   

def publish(count_measurements, count_records, ttime):
   client = InfluxDBClient(host='http://np04-srv-001.cern.ch', port=8086)
   client.create_database('prometheus_lola')
   measurements=[]
   measurement_name = 'm1'
   number_of_points = 250000 #count_records
   data_end_time = int(time.time() * 1000) #milliseconds
   data = []
   payloads = []
   for i in range(count_measurements):
      current_point_time = data_end_time
      time_now=int(time.time()*1000) #miliseconds
      time_from_epoch = 1284045544000 * 1000 # 1 year
         if time_from_epoch > time_now :
            break
      for j in range(number_of_points):
         sec_offset = j * 1000  # 1 second jump
         current_point_time = current_point_time - sec_offset
         data.append("{measurement},host={host},process={process}i {timestamp}"
            .format(measurement='daq-'+str(int(sys.argv[5]))+'-metric-'+str(i),
                    host=str(sys.argv[4]),
                    process=str(int(sys.argv[5])),        
                    timestamp=current_point_time)
   
   client_write_start_time = time.perf_counter()

   client.write_points(data, database='prometheus_lola', time_precision='ms', batch_size=10000, protocol='line')

   client_write_end_time = time.perf_counter()

   print("Application Write: {time}s".format(time=client_write_end_time - client_write_start_time))
   

#count = input ("Please enter the number of metrics per process:")
#count = int(count)
count_measurements = int(sys.argv[1])
print(count_measurements)

#count = input ("Please enter the number of records in one metric:")
#count = int(count_records)
count_records = int(sys.argv[2])
print(count_records)


#ttime = input ("Please enter the total time in minutes:")
#ttime = int(ttime)*60
ttime = int(sys.argv[3])*60
print(ttime)



publish(count_measurements, count_records, ttime)



