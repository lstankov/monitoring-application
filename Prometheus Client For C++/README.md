# First prototype application using Prometheus Client C++
Implemented: 
- execution count (number of output writes)
- execution time - duration of writes in ms

### Client Library can be founf on:
[GITHUB](https://github.com/jupp0r/prometheus-cpp)


## Tools and sw needed for building the application 
- wget
- cmake3
- curl
- zlib

 - install already compiled RPM package provided in this repository
```sh
rpm -i prometheus-cpp-0.9.0-1.x86_64.rpm
```


## Compile the application
The source file (main.cpp )can be found in this repository as well as the CMakeLists.txt file

To compile the example code run the following:
```sh
mkdir build
cd build
cmake3 .. -Dprometheus-cpp_DIR=/usr/lib64/cmake/prometheus-cpp
make
```

## Run the application
```sh
./main -h
./main cf=/path-to-config.txt/ fs=1024 bs=5
#wget 127.0.0.1:8080/metrics
```
## In separate terminal connect to 127.0.0.1:8080
```sh
wget 127.0.0.1:8080/metrics
#open the url link: 127.0.0.1:8080/metrics
```



----

