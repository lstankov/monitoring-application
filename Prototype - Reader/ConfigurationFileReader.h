
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

class ConfigurationFileReader {
    public:
      string configFile;
      string fileName;
      string outputPath;
      int fileSize;
      int blockSize;
      int totalIO;
      int execTime;
      map<string, string> properties;


     int readConfig(string configFile){
     
     initializeProperties();
     ifstream cFile (configFile);
     if (cFile.is_open())
     {
        string line;
	string part,key,value;
	properties=map<string, string>();

	  while(getline(cFile, line)){
          if (line.find("#") != 0 && !line.empty() && line.find("="))
                {
                    part = line.substr(0, line.find("#"));
                    key = part.substr(0, line.find("="));
                    value = part.substr(line.find("=") + 1);
                    properties[key]=value;
                }

        }
	
    if (properties.count("fileName") > 0) fileName=properties["fileName"]; 
    if (properties.count("outputPath")>0) outputPath=properties["outputPath"];
    if (properties.count("fileSize")>0) fileSize=stoi(properties["fileSize"]);
    if (properties.count("blockSize")>0) blockSize=stoi(properties["blockSize"]);
    if (properties.count("totalIO")>0)   totalIO=stoi(properties["totalIO"]);
    if (properties.count("execTime")>0)  execTime=stoi(properties["execTime"]); 
    
	return 0;
        
    }
    else {
        cerr << "Couldn't open config file for reading.\n";
	return 1;
    }

}

void overwrite(int argc,const char** argv){
int i;
int temp;
for (i=2; i<argc; i++){
temp=i;
switch(i){
	case 2: {fileName=argv[2]; break;}
	case 3: {outputPath=argv[3]; break;}
	case 4: {fileSize=stoi(argv[4]); break;}
	case 5: {blockSize=stoi(argv[5]); break;}
	case 6: {totalIO=stoi(argv[6]); break;}
	case 7: {execTime=stoi(argv[7]); break;}

	}
}		
	
}

void initializeProperties()
{
  fileName="";
  outputPath="";
  fileSize=0;
  blockSize=0;
  totalIO=0;
  execTime=0;

}
string get_value(const char* key)
{
    return properties[key];
}


};


