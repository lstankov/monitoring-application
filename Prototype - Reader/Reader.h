
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <vector>
#include <chrono>
#include <map>
#include <thread>

using namespace std;

class Reader {
    public:
    string current_exec_name;
     unsigned long chunkSize;
    unique_ptr<char[]> buffer;

void read_file(const string& outputPath, const string& fileName, int file_size) { 
    current_exec_name = outputPath;
    current_exec_name.append("/");
    current_exec_name.append(fileName);

    ifstream ifs(current_exec_name, std::ifstream::binary);

    int i = 0;
    int file_size_in_bytes=file_size*1024;

    unique_ptr<char[]> buffer(new char[file_size_in_bytes]);

    while (ifs)
    {
	ifs.read(buffer.get(), file_size_in_bytes);
     }

    printf("\nThe file : %s has been read.\n",current_exec_name.c_str());
///test if it works
/*   
    ofstream ofs(current_exec_name+".OUT", std::ios::trunc);
    ofs.write(buffer.get(), file_size_in_bytes);
    printf("\nThe file had been tested.: . \n");
*/
    
}

  
void read_chunks(int FILE_SIZE_IN_KB, int FRAGMENT_SIZE_IN_KB, const string& outputPath, const string& fileName, int totalIO, int execTime)
{
    int chunks=0;

    //Read test file in fragments
    unsigned long chunkSize=FRAGMENT_SIZE_IN_KB*1024; //this is the chunk size in bytes
    unique_ptr<char[]> buffer(new char[chunkSize]);
    int i;
    chunks=FILE_SIZE_IN_KB/FRAGMENT_SIZE_IN_KB+1;
    if(totalIO > chunks)
       cout<<"The number of I/O operations exceededs the number of blocks."<< "Coulden't read the blocks.\n" <<endl;

    else {
        ifstream ifs(current_exec_name);
        if (ifs.is_open()) {
                for(i=0; i<totalIO; i++)
                ifs.read(buffer.get(),chunkSize);
         }
    else
      cout<< "Couldn't open file =:"<< current_exec_name << "for reading the blocks.\n"<<endl;

   ifs.close();
   }

}

void prepare_buffer(unsigned long blockSize ){

  chunkSize = blockSize*1024; //this is the chunk size in bytes
  buffer=unique_ptr<char[]>(new char[chunkSize]);

}



void read_chunk(std::ifstream&  ifs) {     	    
       if (ifs.is_open()) { 
	  ifs.read(buffer.get(),chunkSize);
       } else
         std::cout<< "Couldn't open file =: " << current_exec_name << " for reading the blocks.\n" ;

   }





};

