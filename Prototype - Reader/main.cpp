
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <vector>
#include <chrono>
#include <map>
#include <thread>
#include "ConfigurationFileReader.h"
#include "Reader.h"
#include "args.hxx"

#include <prometheus/counter.h>
#include <prometheus/summary.h>
#include <prometheus/histogram.h>
#include <prometheus/exposer.h>
#include <prometheus/registry.h>

using namespace std;
using namespace prometheus;

//const int FILE_SIZE_IN_KB = 1024; 
//const int FRAGMENT_SIZE_IN_KB=5;

static Histogram::BucketBoundaries CreateLinearBuckets(double start, double end,
                                                       double step) {
  auto bucket_boundaries = Histogram::BucketBoundaries{};
  for (auto i = start; i < end; i += step) {
    bucket_boundaries.push_back(i);
  }
  return bucket_boundaries;
}

int process_arguments(int argc, char** argv, args::ArgumentParser parser, ConfigurationFileReader *conf)
{
   if(argc==1) {
        printf("\nNo Extra Command Line Argument Passed Other Than Program Name\n"); 
    return 1;
    }
 
    parser.LongPrefix("");
    parser.LongSeparator("=");
    args::HelpFlag help(parser, "HELP", "Official help menu", {"help"});
    args::ValueFlag<string> cf(parser, "", "Config file (full path)", {"config_file"}, "");
    args::ValueFlag<string> fn(parser, "", "File name", {"file_name"}, "");
    args::ValueFlag<string> op(parser, "", "Output path", {"output_file"}, "");
    args::ValueFlag<long> fs(parser, "", "File size in kilobytes", {"size"}, 1024);
    args::ValueFlag<long> bs(parser, "", "Block size in kilobytes", {"bs"}, 5);
    args::ValueFlag<long> tio(parser, "", "iTotal input/output (integer)", {"io"}, 0);
    args::ValueFlag<long> et(parser, "", "Execution time in seconds", {"exec_time"},0);
    
    try
    {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Help)
    {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch (args::ValidationError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    if (conf->readConfig(args::get(cf))==1) return 1;
    if (cf) { conf->configFile=args::get(cf); }
    std::cout << "config_file = " << conf->configFile << std::endl; 
    if (fn) { conf->fileName=args::get(fn);}
    std::cout << "file_name = " << conf->fileName << std::endl; 
    if (op) { conf->outputPath=args::get(op);}
    std::cout << "output_file = " << conf->outputPath << std::endl;
    if (fs) { conf->fileSize=args::get(fs);}
    std::cout << "size = " << conf->fileSize << std::endl ;
    if (bs) { conf->blockSize=args::get(bs);}
    std::cout << "bs = " << conf->blockSize << std::endl ;
    if (tio) { conf->totalIO=args::get(tio);}
    std::cout << "io = " << conf->totalIO<< std::endl;
    if (et) { conf->execTime=args::get(et);}
    std::cout << "exec_time = " << conf->execTime << std::endl;

   
}
  

int main(int argc, char** argv)
{     
	
    ConfigurationFileReader *conf=new ConfigurationFileReader();
    args::ArgumentParser parser("This is a first prototype program.");
    
    if (process_arguments(argc,argv, parser, conf)==1)
	return 1;
         
     
     Reader reader;
     reader.read_file(conf->outputPath, conf->fileName,conf->fileSize);


    // create an http server running on port 8080
    Exposer exposer{"127.0.0.1:8080"};
    auto registry = std::make_shared<Registry>();

    auto& counter_family = BuildCounter()
                             .Name("reader_metrics")
                             .Help("Execution count and duration in ms of reading blocks.")
                             .Labels({{"label", "value"}})
                             .Register(*registry);

    auto& call_counter = counter_family.Add({
        {"reader", "count"}});

    auto& execution_timer = counter_family.Add({
        {"reader", "duration in ms"}});

	//summary
    auto& summary_family =
      BuildSummary().Name("writer_summary").Help("").Register(*registry);

    auto& summary = summary_family.Add(
      {}, Summary::Quantiles{
              {0.5, 0.05}, {0.9, 0.01}, {0.95, 0.005}, {0.99, 0.001}});

   //histogram
   auto& histogram_family =
      BuildHistogram().Name("reader_histogram").Help("").Register(*registry);

   auto bucket_boundaries = CreateLinearBuckets(0, 5 - 1, 1);
   auto& histogram = histogram_family.Add({}, bucket_boundaries);
 
  
   //gauge
   auto& gauge_family =
       BuildGauge().Name("reader_gauge").Help("Rate of IO operations per second.").Register(*registry);
   auto& gauge = gauge_family.Add({{"reader", "IO rate per second"}});
   //auto& gauge_iops = gauge_family.Add({{"writes", "IO rate per second different rate"}});
    
   exposer.RegisterCollectable(registry);


  float submit_io = conf->fileSize/conf->blockSize;   

/*  // Check if configuration parameter exists and within limits 
  if (conf->totalIO > submit_io) { 
    cout<<"The number of I/O operations selected exceededs the number of blocks."<< "Coulden't read the blocks.\n" <<endl;
       // TODO: add error and exit
  } else {
    submit_io = conf->totalIO ;
  }
 
  std::cout << "SubmitIO: " << submit_io << "\n" ;*/


////testing reading
std::cout << "Started reading. \n";
  reader.prepare_buffer(conf->blockSize);
  ifstream ifs(reader.current_exec_name);

unsigned long readSize=conf->blockSize; //mb

//while(true) {
  for(int i=0; i<submit_io; i++) {
    auto start = std::chrono::system_clock::now();

    reader.read_chunk(ifs);

    auto elapsed_seconds = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::system_clock::now() - start);

execution_timer.Increment(elapsed_seconds.count());
    summary.Observe(elapsed_seconds.count());
    histogram.Observe(elapsed_seconds.count());
    call_counter.Increment();
    gauge.Set(call_counter.Value()/execution_timer.Value()*readSize);
    
  }
  ifs.close();
  std::cout << "Finished reading.\n";
  std::cout << gauge.Value();



  // Deallocating configuration file reader
  delete(conf);

  return 0;
}
