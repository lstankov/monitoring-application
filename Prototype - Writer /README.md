# Writer prototype

#Requirements
-C++11 comiler

```sh

$ g++ -std=c++11 -o main main.cpp

#To see the help menu. 
$./main -h

    This is a first prototype program.

  OPTIONS:

      help                              Official help menu.
      cf=[Config file ]                 Config file
      fn=[File name]                    File name
      op=[Ouput path]                   Output path
      fs=[File size]                    kilobytes
      bs=[Block size]                   kilobytes
      tio=[Total input/output]          integer
      et=[Execution time]               seconds


$ ./main cf=config.txt

# Overwrite properties from command line
$ ./main cf=config.txt bs=5 fs=1024 et=10 fn=new.txt
cf = config.txt
fn = new.txt
op = /home/lstankovic/Desktop/test
fs = 1024
bs = 5
tio = 3000
et = 10
```





