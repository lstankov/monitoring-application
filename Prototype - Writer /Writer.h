
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <vector>
#include <chrono>
#include <map>
#include <thread>

using namespace std;

class Writer {
    public:

void create_file(const string& filename, int file_size) {

    ofstream ofs(filename, std::ios::trunc);

    int i = 0;
    int file_size_in_bytes=file_size*1024;

    unique_ptr<char[]> buffer(new char[file_size_in_bytes]);


    for (int m = 0; m < file_size_in_bytes; ++m) {
            //buffer[m] = 'a' + (i++ % 8);
	      buffer[m]='\0';
        }

    ofs.write(buffer.get(), file_size_in_bytes);
    
}

  

void write_chunks(int FILE_SIZE_IN_KB, int FRAGMENT_SIZE_IN_KB, const string& outputPath, const string& fileName, int totalIO, int execTime)
{     	    
    int chunks=0;
    string current_exec_name = outputPath;
    current_exec_name.append("/");
    current_exec_name.append(fileName);
  
    printf("\nThe output path of created file is: %s. \n",current_exec_name.c_str());

    //Create test file
    create_file(current_exec_name,FILE_SIZE_IN_KB);
    ifstream fileStream(current_exec_name);
     
    //Write to test file in fragments
    unsigned long chunkSize=FRAGMENT_SIZE_IN_KB*1024; //this is the chunk size in bytes
    unique_ptr<char[]> buffer(new char[chunkSize]);
    int i;
    for(i=0; i<chunkSize; i++)
    	buffer[i]=(char)(i%8+'a');

    chunks=FILE_SIZE_IN_KB/FRAGMENT_SIZE_IN_KB+1;
    if(totalIO > chunks)
       cout<<"The number of I/O operations exceededs the number of blocks."<< "Coulden't write the blocks.\n" <<endl;

    else {
    	ofstream output(current_exec_name);
    	if (output.is_open()) { 
    		for(i=0; i<totalIO; i++)
		output.write(buffer.get(),chunkSize);
         }
    else
      cout<< "Couldn't open file =:"<< current_exec_name << "for writing the blocks.\n"<<endl;

   output.close();
   }

}
};
