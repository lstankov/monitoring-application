
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <vector>
#include <chrono>
#include <map>
#include <thread>
#include "ConfigurationFileReader.h"
#include "Writer.h"
#include "args.hxx"

using namespace std;


//const int FILE_SIZE_IN_KB = 1024; 
//const int FRAGMENT_SIZE_IN_KB=5;

int process_arguments(int argc, char** argv, args::ArgumentParser parser, ConfigurationFileReader *conf)
{
   if(argc==1) {
        printf("\nNo Extra Command Line Argument Passed Other Than Program Name\n"); 
    return 1;
    }
 
    parser.LongPrefix("");
    parser.LongSeparator("=");
    args::HelpFlag help(parser, "HELP", "Official help menu.", {"help"});
    args::ValueFlag<string> cf(parser, "Config file ", "Config file", {"cf"}, "");
    args::ValueFlag<string> fn(parser, "File name", "File name", {"fn"}, "");
    args::ValueFlag<string> op(parser, "Ouput path", "Output path", {"op"}, "");
    args::ValueFlag<long> fs(parser, "File size", "kilobytes", {"fs"}, 1024);
    args::ValueFlag<long> bs(parser, "Block size", "kilobytes", {"bs"}, 5);
    args::ValueFlag<long> tio(parser, "Total input/output", "integer", {"tio"}, 0);
    args::ValueFlag<long> et(parser, "Execution time", "seconds", {"et"},0);
    
    try
    {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Help)
    {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch (args::ValidationError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    if (conf->readConfig(args::get(cf))==1) return 1;
    if (cf) { conf->configFile=args::get(cf); }
    std::cout << "cf = " << conf->configFile << std::endl; 
    if (fn) { conf->fileName=args::get(fn);}
    std::cout << "fn = " << conf->fileName << std::endl; 
    if (op) { conf->outputPath=args::get(op);}
    std::cout << "op = " << conf->outputPath << std::endl;
    if (fs) { conf->fileSize=args::get(fs);}
    std::cout << "fs = " << conf->fileSize << std::endl ;
    if (bs) { conf->blockSize=args::get(bs);}
    std::cout << "bs = " << conf->blockSize << std::endl ;
    if (tio) { conf->totalIO=args::get(tio);}
    std::cout << "tio = " << conf->totalIO<< std::endl;
    if (et) { conf->execTime=args::get(et);}
    std::cout << "et = " << conf->execTime << std::endl;

   
}
  

int main(int argc, char** argv)
{     
	
    ConfigurationFileReader *conf=new ConfigurationFileReader();
    args::ArgumentParser parser("This is a first prototype program.");
    
    if (process_arguments(argc,argv, parser, conf)==1)
	return 1;
    
    Writer writer;
    writer.write_chunks(conf->fileSize, conf->blockSize, conf->outputPath, conf->fileName, conf->totalIO, conf->execTime);

   delete(conf);

   return 0;
}

