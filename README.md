# Monitoring application

#Requirements
-C++11 comiler

#To build the project and to install the libraries and headers it is necessary to have cmake higher version
#Download CMake from: https://cmake.org/download/
```sh
$ wget https://cmake.org/files/v3.12/cmake-3.12.3.tar.gz
$ tar zxvf cmake-3.*
$ cd cmake-3.*
$ ./bootstrap --prefix=/usr/local
$ make -j$(nproc)
$ sudo make install
```

#CURL
```sh
$ cd ~/sdk-folder/third-party
$ wget http://curl.haxx.se/download/curl-7.54.0.tar.bz2
$ tar -xvjf curl-7.54.0.tar.bz2
$ cd curl-7.54.0
$ ./configure --with-nghttp2=/usr/local --with-ssl
$ make
$ sudo make install
$ sudo ldconfig
```

#Download source code for the Prometheus Client LIbrary for Modern C++ from:
$ https://github.com/jupp0r/prometheus-cpp
```sh
$ git submodule init
$ git submodule update

$mkdir _build
$ cd _build

#run cmake
$ cmake .. -DBUILD_SHARED_LIBS=ON # or OFF for static libraries

#build
$ make -j 4

#run tests
$ ctest -V
```
#install the libraries and headers
```sh
$ mkdir -p deploy
$ make DESTDIR=`pwd`/deploy install

#After successfully installing libraries the prometheus-client-example.cpp  can be compiled with
# 
#g++ -std=c++11 prometheus-client-example.cpp -I/path-to-deploy/usr/local/include  -L/path-to-deploy/usr/local/lib64
#Note that this successfully includes header files. 
#When referencing prometheus methods, there is a reference not found error as either implementation
#files are not included in include directory or in built shared libraries 
#
#When referencing libraries -L /path-to-/prometheus-cpp/_build/lib it gets same message 
#
```


